$('#drop-zone').hide();
$('#vc-table-zone').hide();
$('#vp-error').hide();
$('#vp-success').hide();
$('#vp_json').hide();

$('#loadKey').on("click", function(e) {
    const k8infra = $('#routeType').val()
    const key = $("#api-key").val();
    if (key !== null) {
        $.ajax({
            type: "GET",
            url: k8infra + "/test-api-key",
            contentType: "application/json; charset=utf-8",
            headers: {"x-api-key": key},
            success: function(result) {
                console.log(result);
                $("#api-zone").hide();
                $('#drop-zone').show();
            },
            error: function(result) {
                console.log(result);
                alert("Key error...");
            }
        });
    }
});

function fileHandler(ev) {
    console.log('File(s) selected')
    
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();

    const files = ev.target.files;
    for (const file of files) {
        if (file.name.endsWith('.json')) {
            processFile(file);
        } else {
            console.error("File is not a JSON");
        }
    }

    $('#vc-table-zone').show();
}

function dropHandler(ev) {
    console.log("File(s) dropped");
  
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  
    if (ev.dataTransfer.items) {
        // Use DataTransferItemList interface to access the file(s)
        [...ev.dataTransfer.items].forEach(item => {
            // If dropped items aren't files, reject them
            if (item.kind === "file") {
                const file = item.getAsFile();
                // Only accept json files
                if (file.name.endsWith('.json')) {
                    processFile(file);
                } else {
                    console.error("File is not a JSON");    
                }
            } else {
                console.error("Item is not a file");
            }
        });
    } else {
        // Use DataTransfer interface to access the file(s)
        [...ev.dataTransfer.files].forEach((file, i) => {
            console.log(`… file[${i}].name = ${file.name}`);
        });
    }

    $('#vc-table-zone').show();
}

function dragOverHandler(ev) {
    console.log("File(s) in drop zone");

    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
}

function processVp(json) {
    var json_str = JSON.stringify(json, null, 2);
    $('#vp_json').html(json_str);
}

function processVc(json) {
    var json_str = JSON.stringify(json, null, 2);
    var is_signed = false;

    //Test if credentialSubject and proof exist, if do anything
    if (json) {
        if (json['credentialSubject'] && json['proof']) {
            is_signed = true;
        }
        if (!is_signed) {
            delete json['type'];
            json_str = JSON.stringify(json, null, 2);
        }
        /*
        if (json['type']) {
            delete json['type'];
            json_str = JSON.stringify(json, null, 2);
        }
        */
    }
    
    var fileTable = document.getElementById('file-table');
    var fileItem = document.createElement('tr');
    fileItem.innerHTML = `<td> # </td>
                    <td> <textarea class="reqJson" rows="10" cols="60%"> ${json_str} </textarea> </td>
                    <td> <button type="button" class="btn btn-danger btn-block delete"> Delete </button> </td>
                    <td> <button type="button" class="btn btn-info btn-block sign"> Sign </button> </td>
                    <td> <textarea class="resJson" rows="10" cols="60%" style="display: none;"> </textarea> </td>
                    <td> <button type="button" class="btn btn-success btn-block download" style="display: none;"> Download </button> </td>`;
    fileTable.tBodies[0].appendChild(fileItem);

    const reqJsonArea = fileItem.querySelector('.reqJson');
    const deleteButton = fileItem.querySelector('.delete');
    const signButton = fileItem.querySelector('.sign');
    const resJsonArea = fileItem.querySelector('.resJson');
    const downloadButton = fileItem.querySelector('.download');

    deleteButton.addEventListener('click', () => {
        fileItem.remove();
    });

    if (is_signed) {
        resJsonArea.innerHTML = json_str;
        signButton.style.display = "none";
        resJsonArea.style.display = "block";
        downloadButton.style.display = "block";
    } else {
        signButton.addEventListener('click', () => {
            const apiKey = $("#api-key").val();
            const url = "https://" + getDomainFromJson(reqJsonArea.innerHTML) + "/api/vc-request"
            console.log("Sign with " + url);
            if (apiKey !== null) {
                $.ajax({
                    type: "PUT",
                    url: url,
                    contentType: "application/json; charset=utf-8",
                    headers: {"x-api-key": apiKey},
                    data: reqJsonArea.innerHTML,
                    success: function(result) {
                        console.log(result);
                        var txt = JSON.stringify(result, null, 2);
                        resJsonArea.innerHTML = txt;
                        signButton.style.display = "none";
                        resJsonArea.style.display = "block";
                        downloadButton.style.display = "block";
                    },
                    error: function(result) {
                        console.log(result);
                        var txt = JSON.stringify(result, null, 2);
                        resJsonArea.style.display = "block";
                        resJsonArea.innerHTML = txt;
                    }
                });
            }
        });
    }

    downloadButton.addEventListener('click', () => {
        const textContent = reqJsonArea.innerHTML;
        const blob = new Blob([textContent], { type: "text/plain" });
        const url = URL.createObjectURL(blob);
        const name = file.name.replace(".json", '_signed.json');
        const a = document.createElement("a");
        
        a.href = url;
        a.download = name; 
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        document.body.removeChild(a);

        downloadButton.style.display = "none";
    });
}

function processFile(file) {
    let fr = new FileReader();
    fr.readAsText(file)
    fr.onload = () => {
        const json_str = fr.result;
        const json = JSON.parse(json_str);
        if (json && json['verifiableCredential']) {
            console.log('VP has been uploaded');
            processVp(json);
            const vcs = json['verifiableCredential'];
            vcs.forEach(vc => {
                processVc(vc);
            });
        } else {
            console.log('VC has been uploaded');
            processVc(vc);
        }
    }
    
    fr.onerror = () => {
        console.error(fr.error)
    }
}


function getUrlFromDid(did) {
    //did = "did:web:agdatahub.provider.demo23.gxfs.fr:participant:eb06c24ac31a16b5f29316b388a742f2487ab626b5d22f23184ea7e8c3a3ed5b/vc/061e5baeb6f3396db6b80344b59192c0880c5fb9bff6d9ba6184171c3a7bf000/data.json"
    return did.split(":")[2]
}

function getUrlFromUri(uri) {
    //uri = "https://agdatahub.provider.demo23.gxfs.fr/service-offering/55407da8-6fdc-4935-8197-286843648373/data.json"
    const url = new URL(uri);
    return url.hostname;
}

function getDomainFromId(id) {
    if (id.startsWith("did:web")) {
        return getUrlFromDid(id)
    } else {
        return getUrlFromUri(id)
    }
}

function getDomainFromJson(json) {
    if (typeof(json) === "string") {
        json = JSON.parse(json);
    }

    if (json && json["@id"]) {
        return getDomainFromId(json["@id"]);
    } else if (json && json["id"]) {
        return getDomainFromId(json["id"]);
    } else {
        console.error("Error with json");
        return null;
    }
}

/*
function signVP() {
    if ($('.sign').length == 0) {
        $('#vp-success').hide();
        $('#vp-error').show();
        $('#vp-error').html('There is not VC...');
        return;
    }
    if ($('.sign:visible').length != 0) {
        $('#vp-success').hide();
        $('#vp-error').show();
        $('#vp-error').html('You need to sign all VCs !');
    } else { 
        var vp = [];
        $(".resJson").each(function( index ) {
            json_str = $(this).text();
            vp.push(JSON.parse(json_str));
        });

        const apiKey = $("#api-key").val();
        const url = "https://" + getDomainFromJson(json_str) + "/api/vp-request"
        
        console.log("Sign with " + url);
        if (apiKey !== null) {
            $.ajax({
                type: "PUT",
                url: url,
                contentType: "application/json; charset=utf-8",
                headers: {"x-api-key": apiKey},
                data: JSON.stringify(vp),
                success: function(result) {
                    console.log(result);
                    var txt = JSON.stringify(result, null, 2);

                    $('#vp-error').hide();
                    $('#vp-success').show();
                    $('#vp-success').html("Success ! VP Signed has been downloaded !");

                    const blob = new Blob([txt], { type: "text/plain" });
                    const url = URL.createObjectURL(blob);
                    const name = 'vp_signed.json';
                    const a = document.createElement("a");
                    
                    a.href = url;
                    a.download = name; 
                    a.style.display = "none";
                    document.body.appendChild(a);
                    a.click();
                    URL.revokeObjectURL(url);
                    document.body.removeChild(a);
                },
                error: function(result) {
                    console.log(result);
                    var txt = JSON.stringify(result, null, 2);
                    $('#vp-success').hide();
                    $('#vp-error').show();
                    $('#vp-error').html(txt);
                }
            });
        }
    } 
}
*/

function genVP() {
    if ($('.sign').length == 0) {
        $('#vp-success').hide();
        $('#vp-error').show();
        $('#vp-error').html('There is not VC...');
        return;
    }
    if ($('.sign:visible').length != 0) {
        $('#vp-success').hide();
        $('#vp-error').show();
        $('#vp-error').html('You need to sign all VCs !');
    } else { 
        var vcs = [];
        $(".resJson").each(function( index ) {
            json_str = $(this).text();
            vcs.push(JSON.parse(json_str));
        });

        vp = JSON.parse($('#vp_json').html());
        vp["verifiableCredential"] = vcs;
        
        $('#vp-error').hide();
        $('#vp-success').show();
        $('#vp-success').html("Success ! VP has been generated !");

        var txt = JSON.stringify(vp, null, 2);

        const blob = new Blob([txt], { type: "text/plain" });
        const url = URL.createObjectURL(blob);
        const name = 'vp_signed.json';
        const a = document.createElement("a");
        
        a.href = url;
        a.download = name; 
        a.style.display = "none";
        document.body.appendChild(a);
        a.click();
        URL.revokeObjectURL(url);
        document.body.removeChild(a);
    } 
}